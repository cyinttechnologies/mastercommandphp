<?php

/*MasterCommand.php
MasterCommand class for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class MasterCommand extends ContainerAwareCommand
{
	protected $settings;
	protected $input;
	protected $output;    
    protected $Doctrine;
 
    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$this->input = $input;
		$this->output = $output;

        try
        {
            $this->loadSettings();
            $this->loadArguments();
            $this->validateInputs();
            $this->subExecute();     
            $this->complete();
        }
        catch(\Exception $Ex)
        {
			$this->handleException($Ex);
		}       

    }

	protected function loadSettings()
	{
		$Doctrine = $this->getContainer()->get('doctrine');
        $this->Doctrine = $Doctrine;
        $this->settings = $Doctrine->getRepository('CYINTSettingsBundle:Setting')->findByNamespace('');
	}

	protected function complete()
	{
		$this->output->writeln('Command completed');
    }

	protected function handleException(\Exception $Ex)
	{
		$this->output->writeln($Ex->getLine() . " " . $Ex->getMessage());
	}

    protected function loadArguments() {}
    protected function validateInputs() {}

}
